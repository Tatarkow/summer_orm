package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.exceptions.MaxLengthExceededException;
import cz.tatarko.java.summer_orm.exceptions.NotNullableException;
import cz.tatarko.java.summer_orm.filters.ForeignFilter;
import cz.tatarko.java.summer_orm.filters.IntegerFilter;
import cz.tatarko.java.summer_orm.filters.StringFilter;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Map;

class SummerORMTest extends BaseTest {
    @Test
    void testGetAll() {
        Result allAuthors = summerORM.getAll(Author.class);
        List<Author> allAuthorsList = allAuthors.getList();

        assertEquals("William Shakespeare", allAuthorsList.get(0).toString());
        assertEquals("Molier", allAuthorsList.get(1).toString());
        assertEquals("Jan Amos Komensky", allAuthorsList.get(2).toString());
    }

    @Test
    void testDelete() {
        try {
            summerORM.delete(romeoAndJulie);
        } catch (NotNullableException | MaxLengthExceededException e) {
            e.printStackTrace();
        }

        summerORM
                .getAll(Book.class)
                .filter("author", ForeignFilter.EQUALS, shakespeare)
                .print();

        String expectedOutput =
                "Othello (William Shakespeare) [1604]\n" +
                        "Hamlet (William Shakespeare) [null]\n";

        assertSameOutput(expectedOutput);
    }

    @Test
    void testOnDelete() {
        try {
            summerORM.delete(komensky);
        } catch (NotNullableException | MaxLengthExceededException e) {
            e.printStackTrace();
        }

        List<Friend> friends = summerORM.getAll(Friend.class).getList();

        assertEquals(1, friends.size());
        assertEquals("Stanislav", friends.get(0).name);
    }

    @Test
    void testNotNullable() {
        assertThrows(
                NotNullableException.class,
                () -> summerORM.create(
                        Author.class,
                        Map.of(
                                "surname", "Bridel"
                        )
                )
        );
    }

    @Test
    void testMaxLength() {
        assertThrows(
                MaxLengthExceededException.class,
                () -> summerORM.create(
                        Author.class,
                        Map.of(
                                "name", "BedrichBedrichBedrichBedrichBedrich",
                                "surname", "Bridel"
                        )
                )
        );
    }
}
