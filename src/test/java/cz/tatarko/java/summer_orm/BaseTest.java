package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.exceptions.MaxLengthExceededException;
import cz.tatarko.java.summer_orm.exceptions.NotNullableException;
import cz.tatarko.java.summer_orm.exceptions.TableCreationException;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Map;

public abstract class BaseTest {
    protected SummerORM summerORM;

    protected Author shakespeare;
    protected Author molier;
    protected Author komensky;

    Book othello;
    Book romeoAndJulie;
    Book hamlet;
    Book lakomec;
    Book tartuffe;
    Book labyrinth;

    protected final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    protected final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restoreStreams() {
        summerORM.finish();

        System.setOut(originalOut);
    }

    private void resetDatabase() {
        summerORM.resetDatabase();
    }

    private void connect() {
        String databaseName = "summer_orm_example";

        summerORM = new SummerORM(databaseName);
    }

    private void createTables() {
        try {
            summerORM.createTables();
        } catch (TableCreationException e) {
            e.printStackTrace();
        }
    }

    private void addAuthors() {
        try {
            shakespeare = summerORM.create(Author.class, Map.of(
                    "name", "William",
                    "surname", "Shakespeare"
            ));

            molier = summerORM.create(Author.class, Map.of(
                    "name", "Molier"
            ));

            komensky = summerORM.create(Author.class, Map.of(
                    "name", "Jan",
                    "middleName", "Amos",
                    "surname", "Komensky"
            ));
        } catch (NotNullableException | MaxLengthExceededException e) {
            e.printStackTrace();
        }
    }

    private void addBooks() {
        try {
            othello = summerORM.create(Book.class, Map.of(
                    "author", shakespeare,
                    "title", "Othello",
                    "publicationYear", 1604
            ));

            romeoAndJulie = summerORM.create(Book.class, Map.of(
                    "author", shakespeare,
                    "title", "Romeo a Julie",
                    "publicationYear", 1595
            ));


            hamlet = summerORM.create(Book.class, Map.of(
                    "author", shakespeare,
                    "title", "Hamlet"
            ));

            lakomec = summerORM.create(Book.class, Map.of(
                    "author", molier,
                    "title", "Lakomec",
                    "publicationYear", 1668
            ));

            tartuffe = summerORM.create(Book.class, Map.of(
                    "author", molier,
                    "title", "Tartuffe",
                    "publicationYear", 1664
            ));

            labyrinth = summerORM.create(Book.class, Map.of(
                    "author", komensky,
                    "title", "Labyrint sveta a raj srdce"
            ));
        } catch (NotNullableException | MaxLengthExceededException e) {
            e.printStackTrace();
        }
    }

    private void addFriends() {
        try {
            summerORM.create(Friend.class, Map.of(
                    "name", "Jan",
                    "favoriteAuthor", komensky
            ));

            summerORM.create(Friend.class, Map.of(
                    "name", "Stanislav",
                    "favoriteAuthor", shakespeare
            ));
        } catch (NotNullableException | MaxLengthExceededException e) {
            e.printStackTrace();
        }
    }

    protected void assertSameOutput(String expectedOutput) {
        assertEquals(
                expectedOutput.replaceAll("\\s+",""),
                outContent.toString().replaceAll("\\s+","")
        );
    }

    @BeforeEach
    public void setUp() {
        connect();
        resetDatabase();
        createTables();
        addAuthors();
        addBooks();
        addFriends();
    }
}
