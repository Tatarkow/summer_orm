package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.TableObject;
import cz.tatarko.java.summer_orm.annotations.*;

/**
 * Helper class (representing a table class) for an example of Summer ORM usage. Check README.md for more details.
 */
public class Book extends TableObject {
    @ForeignField(onDelete=OnDelete.SET_NULL, nullable=true)
    Author author;

    @StringField(maxLength=128)
    String title;

    @IntegerField(nullable=true)
    Integer publicationYear;

    @Override
    public String toString() {
        return String.format("%s (%s) [%s]", title, author, publicationYear);
    }
}
