package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.TableObject;
import cz.tatarko.java.summer_orm.annotations.ForeignField;
import cz.tatarko.java.summer_orm.annotations.OnDelete;
import cz.tatarko.java.summer_orm.annotations.StringField;

/**
 * Helper class (representing a table class) for an example of Summer ORM usage. Check README.md for more details.
 */
public class Friend extends TableObject {
    @StringField(maxLength=32)
    String name;

    @ForeignField(onDelete=OnDelete.DELETE)
    Author favoriteAuthor;

    @Override
    public String toString() {
        return String.format("%s (favorite author: %s)", name, favoriteAuthor);
    }
}
