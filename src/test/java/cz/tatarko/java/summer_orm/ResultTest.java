package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.exceptions.MaxLengthExceededException;
import cz.tatarko.java.summer_orm.exceptions.NotNullableException;
import cz.tatarko.java.summer_orm.filters.ForeignFilter;
import cz.tatarko.java.summer_orm.filters.IntegerFilter;
import cz.tatarko.java.summer_orm.filters.StringFilter;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Map;

class ResultTest extends BaseTest {
    @Test
    void testPrint() {
        Result allAuthors = summerORM.getAll(Author.class);
        allAuthors.print();

        String expectedOutput =
                "William Shakespeare\n" +
                        "Molier\n" +
                        "Jan Amos Komensky\n";

        assertSameOutput(expectedOutput);
    }

    @Test
    void testPrintColumns() {
        Result allAuthors = summerORM.getAll(Author.class);
        allAuthors.print("name", "surname");

        String expectedOutput =
                "name\tsurname\n" +
                        "William\tShakespeare\n" +
                        "Molier\tnull\n" +
                        "Jan\tKomensky\n";

        assertSameOutput(expectedOutput);
    }

    @Test
    void testSort() {
        summerORM
                .getAll(Author.class)
                .sort("name")
                .print();

        String expectedOutput =
                "Jan Amos Komensky\n" +
                        "Molier\n" +
                        "William Shakespeare\n";

        assertSameOutput(expectedOutput);
    }

    @Test
    void testFilter() {
        summerORM
                .getAll(Author.class)
                .filter("surname", StringFilter.IS_NULL, false)
                .print();

        String expectedOutput =
                "William Shakespeare\n" +
                        "Jan Amos Komensky\n";

        assertSameOutput(expectedOutput);
    }

    @Test
    void testSortDescending() {
        summerORM
                .getAll(Book.class)
                .sort("publicationYear", false)
                .print();

        String expectedOutput =
                "Lakomec (Molier) [1668]\n" +
                        "Tartuffe (Molier) [1664]\n" +
                        "Othello (William Shakespeare) [1604]\n" +
                        "Romeo a Julie (William Shakespeare) [1595]\n" +
                        "Hamlet (William Shakespeare) [null]\n" +
                        "Labyrint sveta a raj srdce (Jan Amos Komensky) [null]\n";

        assertSameOutput(expectedOutput);
    }

    @Test
    void testMultipleFilters() {
        summerORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.LESS_THAN, 1670)
                .filter("publicationYear", IntegerFilter.MORE_THAN, 1660)
                .print();

        String expectedOutput =
                "Lakomec (Molier) [1668]\n" +
                        "Tartuffe (Molier) [1664]\n";

        assertSameOutput(expectedOutput);
    }

    @Test
    void testFilterForeignKey() {
        summerORM
                .getAll(Book.class)
                .filter("author", ForeignFilter.EQUALS, shakespeare)
                .print();

        String expectedOutput =
                "Othello (William Shakespeare) [1604]\n" +
                        "Romeo a Julie (William Shakespeare) [1595]\n" +
                        "Hamlet (William Shakespeare) [null]\n";

        assertSameOutput(expectedOutput);
    }

    @Test
    void testDelete() {
        summerORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.IS_NULL, true)
                .delete();

        summerORM
                .getAll(Book.class)
                .print();

        String expectedOutput =
                "Othello (William Shakespeare) [1604]\n" +
                        "Romeo a Julie (William Shakespeare) [1595]\n" +
                        "Lakomec (Molier) [1668]\n" +
                        "Tartuffe (Molier) [1664]\n";

        assertSameOutput(expectedOutput);
    }

    @Test
    void testFirst() {
        Book oldestBook = summerORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.IS_NULL, false)
                .sort("publicationYear")
                .first();

        assertEquals(romeoAndJulie.title, oldestBook.title);
    }

    @Test
    void testLast() {
        Book newestBook = summerORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.IS_NULL, false)
                .sort("publicationYear")
                .last();

        assertEquals(lakomec.title, newestBook.title);
    }

    @Test
    void testSet() {
        try {
            summerORM
                    .get(lakomec)
                    .set(Map.of(
                            "publicationYear", 2668
                    ));
        } catch (NotNullableException | MaxLengthExceededException e) {
            e.printStackTrace();
        }

        int publicationYear = (summerORM.get(lakomec).first()).publicationYear;

        assertEquals(2668, publicationYear);
    }

    @Test
    void testSetMultiple() {
        try {
            summerORM
                    .getAll(Book.class)
                    .set(Map.of(
                            "publicationYear", 2000
                    ))
                    .print();
        } catch (NotNullableException | MaxLengthExceededException e) {
            e.printStackTrace();
        }

        List<Book> books = summerORM.getAll(Book.class).getList();

        assertEquals(6, books.size());

        for (Book book: books) {
            assertEquals(2000, book.publicationYear);
        }
    }
}
