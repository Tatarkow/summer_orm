package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.TableObject;
import cz.tatarko.java.summer_orm.annotations.StringField;

/**
 * Helper class (representing a table class) for an example of Summer ORM usage. Check README.md for more details.
 */
public class Author extends TableObject {
    @StringField(maxLength=32)
    String name;

    @StringField(nullable=true, maxLength=32)
    String surname;

    @StringField(nullable=true, maxLength=32)
    String middleName;

    @Override
    public String toString() {
        StringBuilder fullName = new StringBuilder(name);

        if (middleName != null) {
            fullName.append(String.format(" %s", middleName));
        }

        if (surname != null) {
            fullName.append(String.format(" %s", surname));
        }

        return fullName.toString();
    }
}
