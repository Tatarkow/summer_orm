package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.annotations.ForeignField;
import cz.tatarko.java.summer_orm.annotations.IntegerField;
import cz.tatarko.java.summer_orm.annotations.StringField;
import cz.tatarko.java.summer_orm.exceptions.MaxLengthExceededException;
import cz.tatarko.java.summer_orm.exceptions.NotNullableException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map;

/**
 * Checks whether values can be used for the given model. For instance, it makes sure that null is not assigned into a
 * fields that has nullable set as false.
 */
class Validator {
    private Validator() {}

    private static boolean isNullableValid(Class<?> cl, Map<String, Object> values, boolean update) {
        Field[] fields = cl.getDeclaredFields();

        for (Field field: fields) {
            Annotation[] annotations = field.getDeclaredAnnotations();
            boolean checkNull = shouldCheckNull(annotations);

            if (checkNull) {
                String column = field.getName();

                if (values.containsKey(column)) {
                    Object value = values.get(column);

                    if (value == null) {
                        return false;
                    }
                }
                else {
                    if (!update) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    private static boolean shouldCheckNull(Annotation[] annotations) {
        for (Annotation annotation: annotations) {
            Class<?> annotationType = annotation.annotationType();

            if (annotationType == StringField.class) {
                if (!((StringField) annotation).nullable()) {
                    return true;
                }
            }
            else if (annotationType == IntegerField.class) {
                if (!((IntegerField) annotation).nullable()) {
                    return true;
                }
            }
            else if (annotationType == ForeignField.class) {
                if (!((ForeignField) annotation).nullable()) {
                    return true;
                }
            }
        }

        return false;
    }

    private static boolean isLengthValid(Class<?> cl, Map<String, Object> values) {
        Field[] fields = cl.getDeclaredFields();

        for (Field field: fields) {
            Annotation[] annotations = field.getDeclaredAnnotations();
            int maxLength = getMaxLength(annotations);

            // Zero means arbitrary length.
            if (maxLength > 0) {
                String column = field.getName();

                if (values.containsKey(column)) {
                    String value = (String) values.get(column);

                    if (value.length() > maxLength) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    private static int getMaxLength(Annotation[] annotations) {
        for (Annotation annotation: annotations) {
            Class<?> annotationType = annotation.annotationType();

            if (annotationType == StringField.class) {
                return ((StringField) annotation).maxLength();
            }
        }

        // Zero means arbitrary length.
        return 0;
    }

    static void validate(Class<?> cl, Map<String, Object> values) throws NotNullableException, MaxLengthExceededException {
        if (!Validator.isNullableValid(cl, values, false)) {
            throw new NotNullableException();
        }

        if (!Validator.isLengthValid(cl, values)) {
            throw new MaxLengthExceededException();
        }
    }

    static void validateUpdate(Class<?> cl, Map<String, Object> values) throws NotNullableException, MaxLengthExceededException {
        if (!Validator.isNullableValid(cl, values, true)) {
            throw new NotNullableException();
        }

        if (!Validator.isLengthValid(cl, values)) {
            throw new MaxLengthExceededException();
        }
    }
}
