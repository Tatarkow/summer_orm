package cz.tatarko.java.summer_orm.conditions;

/**
 * Takes care of generation a SQL "where" condition based on a filtering. This condition class is used by IntegerFilter.
 */
public class IntegerCondition extends Condition {
    /**
     * Generates SQL condition for "equals" filter.
     */
    public static String equals(String column, int value) {
        return String.format("%s = %s", column, value);
    }

    /**
     * Generates SQL condition for "less than" filter.
     */
    public static String lessThan(String column, int value) {
        return String.format("%s < %s", column, value);
    }

    /**
     * Generates SQL condition for "less than" filter.
     */
    public static String moreThan(String column, int value) {
        return String.format("%s > %s", column, value);
    }
}
