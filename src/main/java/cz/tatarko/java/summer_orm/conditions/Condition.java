package cz.tatarko.java.summer_orm.conditions;

import cz.tatarko.java.summer_orm.DatabaseHandlers.DatabaseHandler;

/**
 * Takes care of generation a SQL "where" condition based on a filtering. This condition class is shared among all
 * other conditions.
 */
public abstract class Condition {
    private static DatabaseHandler databaseHandler;

    protected Condition() {}

    /**
     * Generates SQL condition for "is null" filter.
     */
    public static String isNull(String column, boolean value) {
        if (value) {
            return databaseHandler.conditionIsNull(column);
        }
        else {
            return databaseHandler.conditionIsNotNull(column);
        }
    }

    public static void setDatabaseHandler(DatabaseHandler databaseHandler) {
        Condition.databaseHandler = databaseHandler;
    }
}
