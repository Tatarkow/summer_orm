package cz.tatarko.java.summer_orm.conditions;

/**
 * Takes care of generation a SQL "where" condition based on a filtering. This condition class is used by ForeignFilter.
 */
public class ForeignCondition extends Condition {
    /**
     * Generates SQL condition for "equals" filter.
     */
    public static String equals(String column, int value) {
        return String.format("%s = %s", column, value);
    }
}
