package cz.tatarko.java.summer_orm.DatabaseHandlers;

import cz.tatarko.java.summer_orm.TableObject;

public interface DatabaseHandler {
    default String conditionIsNull(String column) {
        return String.format("%s IS NULL", column);
    }

    default String conditionIsNotNull(String column) {
        return String.format("%s IS NOT NULL", column);
    }

    default String resultGetListSelectFrom(String table) {
        return String.format("SELECT * FROM %s", table);
    }

    default String resultGetListWhere(String condition) {
        return String.format(" WHERE %s", condition);
    }

    default String resultGetListOrderBy(String sorting) {
        return String.format(" ORDER BY %s", sorting);
    }

    default String resultGetListLimit(int limit) {
        return String.format(" LIMIT %s", limit);
    }

    default String resultSortAscending() {
        return " ASC";
    }

    default String resultSortDescending() {
        return " DESC";
    }

    default String resultDelete(String tableName, String condition) {
        return String.format("DELETE FROM %s WHERE %s", tableName, condition);
    }

    default String resultFilterAnd() {
        return " AND ";
    }

    default String resultSwitchSorting(String sorting) {
        String newSorting = sorting;

        newSorting = newSorting.replace("ASC", "%TMP%");
        newSorting = newSorting.replace("DESC", "ASC");
        newSorting = newSorting.replace("%TMP%", "DESC");

        return newSorting;
    }

    default String resultSwitchSortingZeroLength() {
        return "ID DESC";
    }

    default String resultSetUpdate(String table) {
        return String.format("UPDATE %s SET ", table);
    }

    default String resultSetComma() {
        return ", ";
    }

    default String resultSetEquals(String column, Object value) {
        return String.format("%s = %s", column, value);
    }

    default String resultSetWhere(String condition) {
        return String.format(" WHERE %s", condition);
    }

    default String tableCreatorCreateTable(String tableName) {
        return String.format("CREATE TABLE %s (id SERIAL PRIMARY KEY", tableName);
    }

    default String tableCreatorColumnDefinition(String name, String datatype) {
        return String.format(", %s %s", name, datatype);
    }

    default String tableCreatorBracket() {
        return ")";
    }

    default String tableCreatorDatatypeText() {
        return "TEXT";
    }

    default String tableCreatorDatatypeVarchar(int maxLength) {
        return String.format("VARCHAR(%s)", maxLength);
    }

    default String tableCreatorDatatypeNotNull() {
        return " NOT NULL";
    }

    default String tableCreatorDatatypeInteger() {
        return "INTEGER";
    }

    default String tableCreatorDatatypeIntegerReference(String tableName) {
        return String.format("INTEGER REFERENCES %s(id)", tableName);
    }

    default String tableCreatorDatatypeIntegerReferenceNotNull(String tableName) {
        return String.format("INTEGER REFERENCES %s(id) NOT NULL", tableName);
    }

    default String tableHelperDelete(String tableName, int objectID) {
        return String.format("DELETE FROM %s WHERE id = %s", tableName, objectID);
    }

    default String summerORMCreateComma() {
        return ", ";
    }

    default String summerORMCreateObjectID(Object value) {
        return String.format("'%s'", ((TableObject) value).getID());
    }

    default String summerORMCreateValue(Object value) {
        return String.format("'%s'", value);
    }

    default String summerORMCreateInsert(String tableName, String queryColumns, String queryValues) {
        return String.format("INSERT INTO %s (%s) VALUES (%s)", tableName, queryColumns, queryValues);
    }

    void summerORMConfigureConnection(String databaseName, String user, String password, String host, String port);

    default String summerORMresetDatabase() {
        return "DROP ALL OBJECTS DELETE FILES";
    }
}
