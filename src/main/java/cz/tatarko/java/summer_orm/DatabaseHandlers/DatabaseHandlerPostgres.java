package cz.tatarko.java.summer_orm.DatabaseHandlers;

import cz.tatarko.java.summer_orm.DataSource;

public class DatabaseHandlerPostgres implements DatabaseHandler {
    @Override
    public void summerORMConfigureConnection(String databaseName, String user, String password, String host, String port) {
        String url = String.format("jdbc:postgresql://%s:%s/%s", host, port, databaseName);
        DataSource.configure(url, user, password);
    }
}
