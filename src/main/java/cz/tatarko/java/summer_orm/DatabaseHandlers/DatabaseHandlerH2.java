package cz.tatarko.java.summer_orm.DatabaseHandlers;

import cz.tatarko.java.summer_orm.DataSource;

public class DatabaseHandlerH2 implements DatabaseHandler {
    @Override
    public String tableCreatorDatatypeIntegerReference(String tableName) {
        return String.format("INTEGER NULL REFERENCES %s(id)", tableName);
    }

    @Override
    public String tableCreatorDatatypeIntegerReferenceNotNull(String tableName) {
        return String.format("INTEGER NOT NULL REFERENCES %s(id)", tableName);
    }

    @Override
    public void summerORMConfigureConnection(String databaseName, String user, String password, String host, String port) {
        String url = String.format("jdbc:h2:./%s", databaseName);
        DataSource.configure(url, user, password);
    }
}
