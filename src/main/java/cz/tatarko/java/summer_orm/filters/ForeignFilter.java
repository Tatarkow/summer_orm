package cz.tatarko.java.summer_orm.filters;

/**
 * Filter types that are use to filter the result based on values in foreign fields (i.e. columns).
 */
public enum ForeignFilter implements Filter {
    EQUALS
}
