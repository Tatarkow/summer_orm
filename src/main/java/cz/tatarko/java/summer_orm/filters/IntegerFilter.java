package cz.tatarko.java.summer_orm.filters;

/**
 * Filter types that are use to filter the result based on values in integer fields (i.e. columns).
 */
public enum IntegerFilter implements Filter {
    EQUALS, LESS_THAN, MORE_THAN, IS_NULL
}
