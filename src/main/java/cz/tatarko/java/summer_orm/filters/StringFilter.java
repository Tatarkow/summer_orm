package cz.tatarko.java.summer_orm.filters;

/**
 * Filter types that are use to filter the result based on values in string fields (i.e. columns).
 */
public enum StringFilter implements Filter {
    IS_NULL
}
