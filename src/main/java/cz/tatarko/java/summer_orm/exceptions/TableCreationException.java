package cz.tatarko.java.summer_orm.exceptions;

/**
 * Custom exception for invalid definition of a table class.
 */
public class TableCreationException extends Exception {
    public TableCreationException(String message) {}
}
