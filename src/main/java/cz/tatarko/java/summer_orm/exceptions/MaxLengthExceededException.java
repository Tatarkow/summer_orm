package cz.tatarko.java.summer_orm.exceptions;

/**
 * Custom exception for trying to store longer string that is allowed.
 */
public class MaxLengthExceededException extends Exception {
}
