package cz.tatarko.java.summer_orm.exceptions;

/**
 * Custom exception for trying to store null into a not nullable field.
 */
public class NotNullableException extends Exception {}
