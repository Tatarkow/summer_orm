package cz.tatarko.java.summer_orm;

/**
 * Class that should be inherited by every class that represents a database table.
 */
public abstract class TableObject {
    private int objectID;

    /**
     * Setter for ID, i.e. primary key in the database table.
     */
    public void setID(int value) {
        objectID = value;
    }

    /**
     * Getter for ID, i.e. primary key in the database table.
     */
    public int getID() {
        return objectID;
    }
}
