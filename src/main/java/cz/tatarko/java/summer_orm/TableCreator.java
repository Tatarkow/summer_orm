package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.DatabaseHandlers.DatabaseHandler;
import cz.tatarko.java.summer_orm.annotations.ForeignField;
import cz.tatarko.java.summer_orm.annotations.IntegerField;
import cz.tatarko.java.summer_orm.annotations.StringField;
import cz.tatarko.java.summer_orm.exceptions.TableCreationException;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Takes care of generating tables from table classes, i.e. Java objects.
 */
class TableCreator {
    Connection connection;

    private List<Class<TableObject>> tableClasses;

    private static DatabaseHandler databaseHandler;

    TableCreator(Connection connection) {
        this.connection = connection;
    }

    public static void setDatabaseHandler(DatabaseHandler databaseHandler) {
        TableCreator.databaseHandler = databaseHandler;
    }

    void createTables() throws TableCreationException {
        List<Class<?>> classes = getClasses();
        tableClasses = getTableClasses(classes);

        for (Class<TableObject> tableClass: tableClasses) {
            createTable(tableClass);
        }
    }

    List<Class<TableObject>> getTableClasses() {
        return tableClasses;
    }

    private void createTable(Class<?> tableClass) throws TableCreationException {
        String tableName = TableHelper.getTableName(tableClass);
        StringBuilder query = new StringBuilder();

        query.append(databaseHandler.tableCreatorCreateTable(tableName));

        Field[] fields = tableClass.getDeclaredFields();

        for (Field field: fields) {
            Class<?> fieldAnnotationType = getFieldAnnotationType(field);
            Annotation[] annotations = field.getDeclaredAnnotations();
            boolean validAnnotationFound = false;

            for (Annotation annotation: annotations) {
                if (annotation.annotationType() == fieldAnnotationType) {
                    String databaseDatatype = getDatabaseDatatype(fieldAnnotationType, annotation, field);
                    query.append(databaseHandler.tableCreatorColumnDefinition(field.getName(), databaseDatatype));
                    validAnnotationFound = true;
                }
                else {
                    throw new TableCreationException("Annotation of invalid type");
                }
            }

            if (!validAnnotationFound) {
                throw new TableCreationException("No annotation of invalid type");
            }
        }

        query.append(databaseHandler.tableCreatorBracket());

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(query.toString());
        } catch (SQLException e) {
            System.out.printf("Table %s could not be created%n", tableName);
        }
    }

    private List<Class<?>> getClasses() {
        int stackTraceDepth = 4;

        String callerClassName = getCallerClassName(stackTraceDepth);
        String callerPackagePrefix = getCallerPackagePrefix(callerClassName);
        String callerFilePath = getClass().getClassLoader().getResource(callerClassName).toString();
        String callerFolderPath = getCallerFolderPath(callerFilePath);
        String callerPackagePath = callerFilePath.replace(callerClassName, "");

        URLClassLoader urlClassLoader = getURLClassLoader(callerPackagePath);
        List<Class<?>> classes = new ArrayList<>();

        if (urlClassLoader == null) {
            return classes;
        }

        File folder = new File(callerFolderPath);
        File[] listOfFiles = folder.listFiles();



        for (File file: listOfFiles) {
            if (file.isFile()) {
                String filename = file.getName();
                String classSuffix = ".class";

                if (filename.endsWith(classSuffix)) {
                    String className = callerPackagePrefix + filename.replace(classSuffix, "");

                    try {
                        Class<?> cl = urlClassLoader.loadClass(className);
                        classes.add(cl);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return classes;
    }

    private String getDatabaseDatatype(Class<?> fieldAnnotationType, Annotation fieldAnnotation, Field field) {
        StringBuilder databaseDatatype = new StringBuilder();

        if (fieldAnnotationType == StringField.class) {
            int maxLength = ((StringField) fieldAnnotation).maxLength();

            // Not limited.
            if (maxLength == 0) {
                databaseDatatype.append(databaseHandler.tableCreatorDatatypeText());
            }
            else {
                databaseDatatype.append(databaseHandler.tableCreatorDatatypeVarchar(maxLength));
            }

            boolean nullable = ((StringField) fieldAnnotation).nullable();

            if (!nullable) {
                databaseDatatype.append(databaseHandler.tableCreatorDatatypeNotNull());
            }

        }
        else if (fieldAnnotationType == IntegerField.class) {
            databaseDatatype.append(databaseHandler.tableCreatorDatatypeInteger());

            boolean nullable = ((IntegerField) fieldAnnotation).nullable();

            if (!nullable) {
                databaseDatatype.append(databaseHandler.tableCreatorDatatypeNotNull());
            }
        }
        else if (fieldAnnotationType == ForeignField.class) {
            String tableName = TableHelper.getTableName(field.getType());
            boolean nullable = ((ForeignField) fieldAnnotation).nullable();

            if (nullable) {
                databaseDatatype.append(databaseHandler.tableCreatorDatatypeIntegerReference(tableName));
            }
            else {
                databaseDatatype.append(databaseHandler.tableCreatorDatatypeIntegerReferenceNotNull(tableName));
            }
        }
        else {
            return null;
        }

        return databaseDatatype.toString();
    }

    private Class<?> getFieldAnnotationType(Field field) {
        Class<?> fieldType = field.getType();

        if (fieldType == String.class) {
            return StringField.class;
        }
        else if (fieldType == Integer.class) {
            return IntegerField.class;
        }
        else if (TableObject.class.isAssignableFrom(field.getType())) {
            return ForeignField.class;
        }
        else {
            return null;
        }
    }

    private List<Class<TableObject>> getTableClasses(List<Class<?>> classes) {
        List<Class<TableObject>> newTableClasses = new ArrayList<>();

        for (Class<?> cl: classes) {
            if (TableObject.class.isAssignableFrom(cl)) {
                newTableClasses.add((Class<TableObject>) cl);
            }
        }

        return newTableClasses;
    }

    private String getCallerClassName(int stackTraceDepth) {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        String callerClassName = stackTrace[stackTraceDepth].getClassName().replace('.', '/') + ".class";

        return callerClassName;
    }

    private String getCallerPackagePrefix(String callerClassName) {
        String[] parts = callerClassName.split("/");
        StringBuilder callerPackagePrefix = new StringBuilder();

        for (int i=0; i<parts.length-1; i++) {
            callerPackagePrefix.append(parts[i]);
            callerPackagePrefix.append(".");
        }

        return callerPackagePrefix.toString();
    }

    private URLClassLoader getURLClassLoader(String callerPackagePath) {
        URLClassLoader urlClassLoader = null;

        try {
            URL url = new URL(callerPackagePath);
            URL[] urls = new URL[] {url};

            urlClassLoader = URLClassLoader.newInstance(urls, this.getClass().getClassLoader());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return urlClassLoader;
    }

    private String getCallerFolderPath(String callerFilePath) {
        String[] callerPathParts = callerFilePath.split("/");
        StringBuilder callerFolderPath = new StringBuilder("/");

        for (int i=1; i<callerPathParts.length-1; i++) {
            callerFolderPath.append(callerPathParts[i]);
            callerFolderPath.append("/");
        }

        return callerFolderPath.toString();
    }
}
