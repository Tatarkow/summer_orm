package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.DatabaseHandlers.DatabaseHandler;
import cz.tatarko.java.summer_orm.conditions.Condition;
import cz.tatarko.java.summer_orm.conditions.IntegerCondition;
import cz.tatarko.java.summer_orm.exceptions.MaxLengthExceededException;
import cz.tatarko.java.summer_orm.exceptions.NotNullableException;
import cz.tatarko.java.summer_orm.filters.Filter;
import cz.tatarko.java.summer_orm.filters.ForeignFilter;
import cz.tatarko.java.summer_orm.filters.IntegerFilter;
import cz.tatarko.java.summer_orm.filters.StringFilter;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * One of the two most important classes. User directly interacts with this class to work with the results of queries.
 */
public class Result<T extends TableObject> {
    Connection connection;
    Class<?> tableClass;

    String table;

    StringBuilder condition;
    StringBuilder sorting;
    int limit;

    private static DatabaseHandler databaseHandler;

    Result(Connection connection, Class<?> tableClass) {
        this.connection = connection;
        this.tableClass = tableClass;

        condition = new StringBuilder();
        sorting = new StringBuilder();
        limit = 0;

        table = TableHelper.getTableName(tableClass);
    }

    public static void setDatabaseHandler(DatabaseHandler databaseHandler) {
        Result.databaseHandler = databaseHandler;
    }

    /**
     * Returns a list of all table objects that satisfy the query that have been created before calling this method.
     */
    public List<T> getList() {
        ResultSet resultSet = null;

        try (Statement statement = connection.createStatement()) {
            StringBuilder query = new StringBuilder();
            query.append(databaseHandler.resultGetListSelectFrom(table));

            if (!condition.toString().isEmpty()) {
                query.append(databaseHandler.resultGetListWhere(condition.toString()));
            }

            if (!sorting.toString().isEmpty()) {
                query.append(databaseHandler.resultGetListOrderBy(sorting.toString()));
            }

            if (limit > 0) {
                query.append(databaseHandler.resultGetListLimit(limit));
            }

            resultSet = statement.executeQuery(query.toString());
            List<T> resultList = new ArrayList<>();

            while (resultSet.next()) {
                T object = (T) tableClass.getConstructor().newInstance();
                Field[] fields = tableClass.getDeclaredFields();

                for (Field field: fields) {
                    String fieldName = field.getName();
                    Object value = resultSet.getObject(fieldName);

                    field.setAccessible(true);
                    Class<?> fieldClass = field.getType();

                    if (TableObject.class.isAssignableFrom(fieldClass)) {
                        Result<T> result = new Result<>(connection, fieldClass);
                        value = result.getObjectByID((int) value);
                    }

                    field.set(object, value);
                }

                String fieldName = "id";
                int objectID = resultSet.getInt(fieldName);
                object.setID(objectID);

                resultList.add(object);
            }

            return resultList;

        } catch (SQLException | InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        }
        finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    /**
     * Returns a single object of the given type with the given (database) ID.
     */
    T getObjectByID(int objectID) {
        filter("id", IntegerFilter.EQUALS, objectID);
        List<T> objects = getList();

        T object = objects.get(0);
        object.setID(objectID);

        return object;
    }

    /**
     * Prints all table objects that satisfy the query that have been created before calling this method.
     */
    public void print() {
        List<T> list = getList();

        for (T object: list) {
            System.out.println(object);
        }
    }

    /**
     * Prints the given columns of all table objects that satisfy the query that have been created before calling this
     * method. It prints header (with the names of the columns) as the first line.
     */
    public void print(String... columns) {
        List<T> list = getList();

        StringBuilder row = new StringBuilder();

        for (String column: columns) {
            row.append(String.format("%s\t", column));
        }

        System.out.println(row);

        for (T object: list) {
            row = new StringBuilder();

            for (String column: columns) {
                try {
                    Field field = tableClass.getDeclaredField(column);
                    field.setAccessible(true);
                    Object value = field.get(object);

                    row.append(String.format("%s\t", value));
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

            System.out.println(row);
        }
    }

    /**
     * Sort the result objects by the given column in ascending order.
     */
    public Result<T> sort(String column) {
        return sort(column, true);
    }

    /**
     * Sort the result objects by the given column.
     */
    public Result<T> sort(String column, boolean ascending) {
        if (sorting.length() > 0) {
            sorting.append(", ");
        }

        sorting.append(column);

        if (ascending) {
            sorting.append(databaseHandler.resultSortAscending());
        }
        else {
            sorting.append(databaseHandler.resultSortDescending());
        }

        return this;
    }

    /**
     * Deletes all table objects that satisfy the query that have been created before calling this method.
     */
    public void delete() {
        String tableName = TableHelper.getTableName(tableClass);
        String query = databaseHandler.resultDelete(tableName, condition.toString());

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Keeps in the result only such objects whose values in the given column satisfy the given filter and its value.
     */
    public Result<T> filter(String column, Filter filter, Object value) {
        if (condition.length() > 0) {
            condition.append(databaseHandler.resultFilterAnd());
        }

        String newCondition = "";

        if (filter == StringFilter.IS_NULL) {
           newCondition = Condition.isNull(column, (boolean) value);
        }
        else if (filter == IntegerFilter.EQUALS) {
            newCondition = IntegerCondition.equals(column, (int) value);
        }
        else if (filter == IntegerFilter.LESS_THAN) {
            newCondition = IntegerCondition.lessThan(column, (int) value);
        }
        else if (filter == IntegerFilter.MORE_THAN) {
            newCondition = IntegerCondition.moreThan(column, (int) value);
        }
        else if (filter == IntegerFilter.IS_NULL) {
            newCondition = Condition.isNull(column, (boolean) value);
        }
        else if (filter == ForeignFilter.EQUALS) {
            newCondition = IntegerCondition.equals(column, ((TableObject) value).getID());
        }

        condition.append(newCondition);

        return this;
    }

    /**
     * Returns the first object of the list of result objects. Null is returned if the result does not contains any
     * object.
     */
    public T first() {
        limit = 1;

        List<T> objects = getList();

        if (!objects.isEmpty()) {
            return objects.get(0);
        }
        else {
            return null;
        }
    }

    /**
     * Returns the lst object of the list of result objects. Null is returned if the result does not contains any
     * object.
     */
    public T last() {
        switchSorting();

        return first();
    }

    private void switchSorting() {
        if (sorting.length() > 0) {
            String newSorting = databaseHandler.resultSwitchSorting(sorting.toString());
            sorting = new StringBuilder(newSorting);
        }
        else {
            sorting.append(databaseHandler.resultSwitchSortingZeroLength());
        }
    }

    /**
     * Updates the values of the result objects to match the given values.
     */
    public Result<T> set(Map<String, Object> pairs) throws NotNullableException, MaxLengthExceededException {
        Validator.validateUpdate(tableClass, pairs);

        StringBuilder query = new StringBuilder();
        query.append(databaseHandler.resultSetUpdate(table));

        boolean first = true;

        for (Map.Entry<String, Object> pair: pairs.entrySet()) {
            String column = pair.getKey();
            Object value = pair.getValue();

            if (first) {
                first = false;
            }
            else {
                query.append(databaseHandler.resultSetComma());
            }

            query.append(databaseHandler.resultSetEquals(column, value));
        }

        if (!condition.toString().isEmpty()) {
            query.append(databaseHandler.resultSetWhere(condition.toString()));
        }

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(query.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return this;
    }
}
