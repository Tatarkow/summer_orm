package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.DatabaseHandlers.DatabaseHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Class containing methods that can be accessed from multiple classes, and thus it does not belong to any of them.
 */
class TableHelper {
    private static DatabaseHandler databaseHandler;

    private TableHelper() {}

    static String getTableName(Class<?> tableClass) {
        String[] classNameParts = tableClass.getName().split("\\.");
        String tableName = classNameParts[classNameParts.length - 1];

        return tableName;
    }

    static void delete(TableObject tableObject, Connection connection) {
        String tableName = TableHelper.getTableName(tableObject.getClass());
        int objectID = tableObject.getID();
        String query = databaseHandler.tableHelperDelete(tableName, objectID);

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setDatabaseHandler(DatabaseHandler databaseHandler) {
        TableHelper.databaseHandler = databaseHandler;
    }
}
