package cz.tatarko.java.summer_orm.annotations;

/**
 * Defines what should happen when the foreign objects gets deleted.
 */
public enum OnDelete {
    DELETE,
    SET_NULL,
}
