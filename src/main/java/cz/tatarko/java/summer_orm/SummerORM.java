package cz.tatarko.java.summer_orm;

import cz.tatarko.java.summer_orm.DatabaseHandlers.DatabaseHandler;
import cz.tatarko.java.summer_orm.annotations.ForeignField;
import cz.tatarko.java.summer_orm.annotations.OnDelete;
import cz.tatarko.java.summer_orm.conditions.Condition;
import cz.tatarko.java.summer_orm.exceptions.MaxLengthExceededException;
import cz.tatarko.java.summer_orm.exceptions.NotNullableException;
import cz.tatarko.java.summer_orm.exceptions.TableCreationException;
import cz.tatarko.java.summer_orm.filters.ForeignFilter;
import cz.tatarko.java.summer_orm.filters.IntegerFilter;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * One of the two most important classes. User directly interacts with this class to work with the database.
 */
public class SummerORM {
    String defaultHost = "localhost";
    String defaultPort = "5432";

    Connection connection;
    TableCreator tableCreator;

    DatabaseHandler databaseHandler;

    /**
     * Creates a connection with the database.
     */
    public SummerORM(String databaseName, String user, String password, String host, String port) {
        initialize(databaseName, user, password, host, port);
    }

    /**
     * Creates a connection with the database.
     */
    public SummerORM(String databaseName, String user, String password, String host) {
        initialize(databaseName, user, password, host, defaultPort);
    }

    /**
     * Creates a connection with the database.
     */
    public SummerORM(String databaseName, String user, String password) {
        initialize(databaseName, user, password, defaultHost, defaultPort);
    }

    /**
     * Creates a connection with the database.
     */
    public SummerORM(String databaseName) {
        String user = null;
        String password = null;

        initialize(databaseName, user, password, defaultHost, defaultPort);
    }

    private void initialize(String databaseName, String user, String password, String host, String port) {
        setDatabaseHandler();
        configureConnection(databaseName, user, password, host, port);
        connect();
    }

    private void connect() {
        try {
            connection = DataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void finish() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void setDatabaseHandler() {
        ServiceLoader<DatabaseHandler> serviceLoader = ServiceLoader.load(DatabaseHandler.class);
        databaseHandler = serviceLoader.iterator().next();

        Condition.setDatabaseHandler(databaseHandler);
        Result.setDatabaseHandler(databaseHandler);
        TableCreator.setDatabaseHandler(databaseHandler);
        TableHelper.setDatabaseHandler(databaseHandler);
    }

    /**
     * Creates a new object of the given type and with the given values.
     */
    public <T extends TableObject> T create(Class<T> cl, Map<String, Object> values) throws NotNullableException, MaxLengthExceededException {
        if (!TableObject.class.isAssignableFrom(cl)) {
            return null;
        }

        Validator.validate(cl, values);

        StringBuilder queryColumns = new StringBuilder();
        StringBuilder queryValues = new StringBuilder();

        Field[] fields = cl.getDeclaredFields();
        boolean isFirst = true;

        for (Field field: fields) {
            String column = field.getName();

            if (values.containsKey(column)) {
                Object value = values.get(column);

                if (!isFirst) {
                    queryColumns.append(databaseHandler.summerORMCreateComma());
                    queryValues.append(databaseHandler.summerORMCreateComma());
                }

                queryColumns.append(column);

                if (value instanceof TableObject) {
                    queryValues.append(databaseHandler.summerORMCreateObjectID(value));
                }
                else {
                    queryValues.append(databaseHandler.summerORMCreateValue(value));
                }
            }

            if (isFirst) {
                isFirst = false;
            }
        }

        String tableName = TableHelper.getTableName(cl);
        String query = databaseHandler.summerORMCreateInsert(tableName, queryColumns.toString(), queryValues.toString());
        int objectID = 0;
        ResultSet resultSet = null;

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            resultSet = statement.getGeneratedKeys();

            if(resultSet.next()){
                objectID = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return getObjectByID(cl, objectID);
    }

    /**
     * Returns all objects of the given types wrapped inside Result object.
     */
    public <T extends TableObject> Result<T> getAll(Class<T> tableClass) {
        return new Result<>(connection, tableClass);
    }

    /**
     * Returns a single object of the given type with the given (database) ID.
     */
    public <T extends TableObject> T getObjectByID(Class<T> tableClass, int objectID) {
        Result<T> result = getAll(tableClass);
        T object = result.getObjectByID(objectID);

        return object;
    }

    /**
     * Returns the given object wrapped inside a Result object.
     */
    public <T extends TableObject> Result<T> get(T tableObject) {
        Class<T> tableClass = (Class<T>) tableObject.getClass();
        int objectID = tableObject.getID();

        Result<T> result = getAll(tableClass);
        result.filter("id", IntegerFilter.EQUALS, objectID);

        return result;
    }

    /**
     * Deletes the given object from the database.
     */
    public void delete(TableObject tableObject) throws NotNullableException, MaxLengthExceededException {
        resolveForeignKeys(tableObject);
        
        TableHelper.delete(tableObject, connection);
    }

    private void resolveForeignKeys(TableObject tableObject) throws NotNullableException, MaxLengthExceededException {
        Class<? extends TableObject> foreignKeyClass = tableObject.getClass();
        List<Class<TableObject>> tableClasses = tableCreator.getTableClasses();

        for (Class<TableObject> tableClass: tableClasses) {
            Field[] fields = tableClass.getDeclaredFields();

            for (Field field: fields) {
                if (field.getType() == foreignKeyClass) {
                    Result<TableObject> objectsToChanged = getAll(tableClass);
                    objectsToChanged = objectsToChanged.filter(field.getName(), ForeignFilter.EQUALS, tableObject);

                    OnDelete onDelete = field.getAnnotation(ForeignField.class).onDelete();

                    if (onDelete == OnDelete.SET_NULL) {
                        // Null cannot be used in "Map.of".
                        HashMap<String, Object> pairs = new HashMap<>();
                        pairs.put(field.getName(), null);

                        objectsToChanged.set(pairs);
                    }
                    else if (onDelete == OnDelete.DELETE) {
                        objectsToChanged.delete();
                    }
                }
            }
        }
    }

    private void configureConnection(String databaseName, String user, String password, String host, String port) {
        databaseHandler.summerORMConfigureConnection(databaseName, user, password, host, port);
    }

    /**
     * Generates a database table for each class that inherits the TableObject class and that is in the same folder as
     * the class from which the method was called.
     */
    public void createTables() throws TableCreationException {
        tableCreator = new TableCreator(connection);
        tableCreator.createTables();
    }

    /**
     * Resets database. It is useful e.g. for testing.
     */
    public void resetDatabase() {
        try (Statement statement = connection.createStatement()) {
            statement.execute(databaseHandler.summerORMresetDatabase());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
